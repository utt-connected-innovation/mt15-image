
Pour construire et pousser l'image (sans passer par gitlab CI/CD) :

```
docker login registry.gitlab.com
docker build -t registry.gitlab.com/utt-connected-innovation/mt15-image .
docker push registry.gitlab.com/utt-connected-innovation/mt15-image
```